from flask import *

app = Flask(__name__)

#Crear una nueva ruta @app.route

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/saludar')
def saludar():
    return("Hola Mundo")

@app.route('/despedir')
def despedir():
    return("Adiós Mundo")

@app.route('/sumar', methods=['POST'])
def sumar():
    misDatos = request.get_json()
    a = misDatos['numero1']
    b = misDatos['numero2']
    resultado = a + b

    return jsonify({
        "resultado":resultado
    })
    
@app.route('/restar', methods=['POST'])
def restar():
    misDatos = request.get_json()
    c = misDatos['numero1']
    d = misDatos['numero2']
    resultado = c - d

    return jsonify({
        "resultado":resultado
    })
    
@app.route('/multiplicar', methods=['POST'])
def multiplicar():
    misDatos = request.get_json()
    e = misDatos['numero1']
    f = misDatos['numero2']
    resultado = e * f

    return jsonify({
        "resultado":resultado
    })

@app.route('/dividir', methods=['POST'])
def dividir():
    misDatos = request.get_json()
    g = misDatos['numero1']
    h = misDatos['numero2']
    resultado = g / h

    return jsonify({
        "resultado":resultado
    })
    
@app.route('/modulo', methods=['POST'])
def modulo():
    misDatos = request.get_json()
    i = misDatos['numero1']
    j = misDatos['numero2']
    resultado = i % j

    return jsonify({
        "resultado":resultado
    })

#Crear un servidor y ejecutar
app.run()